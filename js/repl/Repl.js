import { start } from 'repl';
import { inspect, promisify } from 'util';
import { homedir } from 'os';
import { join } from 'path';

import * as libModel from '../src/index.js';
import * as utils from '@omniblack/utils';

const histPath = join(homedir(), '.modelReplHist');
inspect.defaultOptions.depth = Infinity;

(async function() {
    const model = new libModel.Model();

    await model.structs.loadStruct('./test.yaml');
    await model.structs.loadStruct('./child.yaml');

    const repl = start();
    const setupHist = promisify(repl.setupHistory).bind(repl);
    await setupHist(histPath);

    Object.assign(repl.context, libModel);
    Object.assign(repl.context, utils);
    repl.context.indiv = model.structs.get('test')();
    const filled = model.structs.get('test')({
        field1: 'test',
        field2: 1,
        date_field: new Date(),
        child: {
            child_field1: 'test',
            child_field2: 1,
        },
    });

    repl.context.filled_indiv = filled;
    repl.context.Date = Date;
})();

