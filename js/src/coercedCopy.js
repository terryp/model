import { T } from '@omniblack/localization';

import { visitLeaves } from './walker.js';
import { map } from './mapper.js';
import { toAdapter, fromAdapter } from './adapters/index.js';
import { getModel } from './Structs.js';
import { throwIfInvalid } from './throw.js';

export function coerceTo(indiv, adapterName) {
    throwIfInvalid(indiv, T`Cannot coerce invalid indiv.`);

    const model = getModel(indiv);
    return map({ indiv, ...visitLeaves }, ({ value, fieldDef }) => {
        const type = model.types.get(fieldDef.type);
        return toAdapter(adapterName, value, type);
    });
}


export function coerceFrom(rec, adapterName, structName, model) {
    const Struct = model.structs.get(structName);
    const indiv = new Struct(rec);
    const coercedRec = map({ indiv, ...visitLeaves }, ({ value, fieldDef }) => {
        const type = model.types.get(fieldDef.type);
        return fromAdapter(adapterName, value, type);
    });

    return new Struct(coercedRec);
}

