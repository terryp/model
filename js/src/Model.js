import { Types } from './types.js';
import { Structs } from './Structs.js';

export function Model() {
    if (!(this instanceof Model)) {
        return new Model();
    }

    this.types = new Types();
    this.structs = new Structs(this);
}
