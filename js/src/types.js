/**
 * @module lib/model/types.js
 * @extends module:lib/model
 */
import { UnknownType, NotImplemented, Code } from '@omniblack/error';
import { merge, invert, has } from 'lodash-es';
import { T } from '@omniblack/localization';

import { string } from './types/string.js';
import { number } from './types/number.js';
import { date } from './types/date.js';
import { integer } from './types/int.js';
import { ValidationResult, ValidationMessage } from './validationResult.js';

const comparatorsReflections = {
    '<': '>',
    '<=': '>=',
    '==': '==',
    '!=': '!=',
};

merge(comparatorsReflections, invert(comparatorsReflections));

const allComparators = Object.values(comparatorsReflections);

export function Types() {
    if (!(this instanceof Types)) {
        return new Types();
    }

    const types = new Map();
    const typeExtensions = new Map();

    /**
     * @typedef {Object} Type
     * @property {Adapters} adapters - The  to use for the type.
     * @property {Comparators} comparators - The comparators implementations for
     *  this type.
     * @property {Validator} validator - The  validator for the type.
     * @property {Struct} attributes - The attributes of the type.
     *      An indiv of attributes will be added to the FieldDef on the `with`
     *      property.
     */

    function register(
        {
            name: typeName,
            adapters,
            comparators,
            validator,
            attributes,
        },
    ) {
        // TODO validate type
        if (types.has(typeName)) {
            throw new Code(T`${typeName} has already been declared.`);
        }

        types.set(typeName, {
            name: typeName,
            adapters,
            comparators,
            validator,
            attributes,
        });
    }

    this.register = register;

    function extend({
        name: typeName,
        adapters,
        comparators,
        validator,
        attributes,
    }) {
        if (!typeExtensions.has(typeExtensions)) {
            typeExtensions.set(typeName, {
                adapters: {},
                comparators: createComparatorExtensionBase(),
                validator: [],
                attributes: [],
            });
        }

        const typeExt = typeExtensions.get(typeName);

        const extAdapters = typeExt.adapters;

        for (const [adapterName, value] of Object.entries(adapters)) {
            if (!has(extAdapters, adapterName)) {
                extAdapters[adapterName] = [];
            }

            if (typeof value === 'boolean') {
                extAdapters[adapterName] = [value];
            } else {
                extAdapters[adapterName].push(value);
            }
        }

        const extComparators = typeExt.comparators;

        for (const [comparatorName, comparatorFunc]
            of Object.entries(comparators)) {
            extComparators[comparatorName].push(comparatorFunc);
        }

        typeExt.validator.push(validator);

        typeExt.attributes.push(...attributes);
    }

    this.extend = extend;

    // register base types
    register(string);
    register(number);
    register(date);

    if (globalThis.BigInt) {
        register(integer);
    }

    function get(typeName) {
        if (types.has(typeName)) {
            return types.get(typeName);
        } else {
            throw new UnknownType(typeName);
        }
    }

    this.get = get;

    function validate(typeName, args, path) {
        const { validator } = get(typeName);

        const notPresent = args.value === null || args.value === undefined;
        if (notPresent) {
            if (args.required) {
                const message = T`"${path}" is required`;
                const validationMessage = new ValidationMessage({
                    message,
                    path,
                });
                return new ValidationResult(false, [validationMessage]);
            }
        }

        return validator(args, path);
    }

    this.validate = validate;



    // == will compare base javascript type
    // as if compared with the native operator ===
    /**
     * @typedef {object} typedValue
     * @property {string} type - the type of the value
     * @property {*} value = The value
     */
    // TODO: add a helper class that can implant some missing comparators
    // with the implemented ones see functools.total_ordering
    /**
     * Compare two values using the declared types.
     * @param {typedValue} typedValueLeft - The left value to compare.
     * @param {string} operator - The operator to use for comparison.
     * @param {typedValue} typedValueRight - The right value to compare.
     * @throws {NotImplemented}
     * @returns {boolean}
     */
    function compare(typedValueLeft, operator, typedValueRight) {
        const { comparators: comparatorsRight } = get(typedValueRight.type);
        const { comparators: comparatorsLeft } = get(typedValueLeft.type);
        const opRight = comparatorsRight[operator];
        const opLeft = comparatorsLeft[operator];

        try {
            return opRight(typedValueLeft, typedValueRight);
        } catch (err) {
            if (err instanceof NotImplemented) {
                try {
                    return opLeft(typedValueLeft, typedValueRight);
                } catch (err) {
                    if (err instanceof NotImplemented) {
                        // TODO add a message
                        throw new NotImplemented();
                    } else {
                        throw err;
                    }
                }
            } else {
                throw err;
            }
        }
    }

    this.compare = compare;

    function createInvoke(...funcs) {
        return (...args) => invoke(funcs, args);
    }

    function invoke(funcs, args) {
        const [first, ...rest] = funcs;
        if (rest.length > 0) {
            args = [...args, createInvoke(funcs)];
        }

        return first(...args);
    }

    function finalize() {
    }

    this.finalize = finalize;
}


function createComparatorExtensionBase() {
    return Object.fromEntries(
        allComparators.map((comparator) => [comparator, []]),
    );
}
