import { DotPath } from '@omniblack/utils';
import { Code } from '@omniblack/error';
import { T } from '@omniblack/localization';

import { getStructName, getModel } from './Structs.js';

export function getFieldDefAtPath(indiv, path) {
    if (!(path instanceof DotPath)) {
        path = DotPath.fromString(path);
    }

    const model = getModel(indiv);
    const rootStructName = getStructName(indiv);
    let structDef = model.structs.getDef(rootStructName);
    let field;

    for (const segment of path) {
        field = structDef.fields.find(({ name }) => name === segment);
        if (!field) {
            throw new Code(
                T`${path} is not a valid path for ${rootStructName}`,
            );
        }

        if (field.type === 'child') {
            const structName = field.attrs.structName;
            structDef = model.structs.getDef(structName);
        }
    }

    return field;
}

