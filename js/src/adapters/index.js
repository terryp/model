import { has, get } from 'lodash-es';

function createAdapterConverter(direction) {
    return function toAdapter(
        adapterName,
        value,
        typeDef,
        __originalAdapter = adapterName,
    ) {
        const typeAdapters = typeDef.adapters;

        const adapter = get(typeAdapters, adapterName);

        if (has(adapter, direction)) {
            const convertedValue = adapter[direction](value);
            return convertedValue;
        } else if (adapterName !== 'string') {
            return toAdapter('string', value, typeDef, __originalAdapter);
        }

        // If there is no registered adapter to this type
        // for this adapter or string so we will just return it
        // the consumer will have to handle it or error.
        return value;
    };
}

export const toAdapter = createAdapterConverter('to');
export const fromAdapter = createAdapterConverter('from');
