import spec from 'es-abstract';
import { NotImplemented } from '@omniblack/error';

import { defaultComparators } from './baseComparators.js';

const { toPrimitive } = spec;

export function valueOfComparators(typeNames) {
    if (typeof typeNames === 'string') {
        typeNames = new Set([typeNames]);
    }

    function checkTypes(typeLeft, typeRight) {
        if (!typeNames.has(typeLeft) || !typeNames.has(typeRight)) {
            throw new NotImplemented();
        }
    }

    const comparators = defaultComparators(typeNames);


    comparators['=='] = function(
        { type: typeLeft, value: valueLeft },
        { type: typeRight, value: valueRight },
    ) {
        checkTypes(typeLeft, typeRight);

        return toPrimitive(valueLeft) === toPrimitive(valueRight);
    };

    comparators['!='] = function(
        { type: typeLeft, value: valueLeft },
        { type: typeRight, value: valueRight },
    ) {
        checkTypes(typeLeft, typeRight);

        return toPrimitive(valueLeft) !== toPrimitive(valueRight);
    };
}




