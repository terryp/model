import { T } from '@omniblack/localization';
import parseISO from 'date-fns/parseISO/index.js';

import { ValidationResult, ValidationMessage } from '../validationResult.js';
import { valueOfComparators } from './valueOfComparators.js';


export const date = {
    name: 'date',
    adapters: {
        string: {
            to: (value) => value.toISOString(),
            from: (string) => parseISO(string),
        },
    },
    comparators: valueOfComparators('date'),
    validator({ value }, path) {
        if (!(value instanceof Date) && !Number.isNaN(value.valueOf())) {
            const msg = new ValidationMessage({
                message:  T`${value} is not a valid date.`,
                path,
            });
            return new ValidationResult(false, [msg]);
        }
        return new ValidationResult(true);
    },
};
