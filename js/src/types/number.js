import { T } from '@omniblack/localization';
import { isNil } from 'lodash-es';


import { ValidationResult, ValidationMessage } from '../validationResult.js';
import { defaultComparators } from './baseComparators.js';

export const number = {
    name: 'number',
    comparators: defaultComparators('number'),
    validator({ value, min, max }, path) {
        if (typeof value !== 'number' && !Number.isNaN(value)) {
            const msg = new ValidationMessage({
                message:  T`${value} is not a valid number.`,
                path,
            });
            return new ValidationResult(false, [msg]);
        }

        const otherMessages = [];

        if (!isNil(min) && min > value) {
            const msg = new ValidationMessage({
                message: T`${value} must not be less than ${min}.`,
                path,
            });
            otherMessages.push(msg);
        }

        if (!isNil(max) && max < value) {
            const msg = new ValidationMessage({
                message: T`${value} must not be greater than ${max}.`,
                path,
            });
            otherMessages.push(msg);
        }

        if (otherMessages.length > 0) {
            return new ValidationResult(false, otherMessages);
        }

        return new ValidationResult(true);
    },
    attributes: [
        {
            name: 'min',
            display: {
                en: 'Minimum',
            },
            type: 'number',
            assumed: Number.MIN_SAFE_INTEGER,
        },
        {
            name: 'max',
            display: {
                en: 'Maximum',
            },
            type: 'number',
            assumed: Number.MAX_SAFE_INTEGER,
        },
    ],
};
