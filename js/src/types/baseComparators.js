import { NotImplemented } from '@omniblack/error';

export function defaultComparators(typeNames) {
    if (typeof typeNames === 'string') {
        typeNames = new Set([typeNames]);
    }

    function checkTypes(typeLeft, typeRight) {
        if (!typeNames.has(typeLeft) || !typeNames.has(typeRight)) {
            throw new NotImplemented();
        }
    }

    return {
        '<': function(
            { type: typeLeft, value: valueLeft },
            { type: typeRight, value: valueRight },
        ) {
            checkTypes(typeLeft, typeRight);
            return valueLeft < valueRight;
        },
        '<=': function(
            { type: typeLeft, value: valueLeft },
            { type: typeRight, value: valueRight },
        ) {
            checkTypes(typeLeft, typeRight);
            return valueLeft <= valueRight;
        },
        '>': function(
            { type: typeLeft, value: valueLeft },
            { type: typeRight, value: valueRight },
        ) {
            checkTypes(typeLeft, typeRight);
            return valueLeft > valueRight;
        },
        '>=': function(
            { type: typeLeft, value: valueLeft },
            { type: typeRight, value: valueRight },
        ) {
            checkTypes(typeLeft, typeRight);
            return valueLeft >= valueRight;
        },
        '==': function(
            { type: typeLeft, value: valueLeft },
            { type: typeRight, value: valueRight },
        ) {
            checkTypes(typeLeft, typeRight);
            return valueLeft === valueRight;
        },
        '!=': function(
            { type: typeLeft, value: valueLeft },
            { type: typeRight, value: valueRight },
        ) {
            checkTypes(typeLeft, typeRight);
            return valueLeft !== valueRight;
        },
    };
}

