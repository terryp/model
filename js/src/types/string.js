import { defaultComparators } from './baseComparators.js';
import { T } from '@omniblack/localization';
import { ValidationMessage, ValidationResult } from '../validationResult.js';
import { isNil } from 'lodash-es';

export const string = {
    name: 'string',
    adapters: {
        string: {
            to: (value) => value,
            from: (value) => value,
        },
    },
    comparators: defaultComparators('string'),
    validator({ value, allowedChars, minLen, maxLen }, path) {
        if (typeof value !== 'string') {
            const message = new ValidationMessage({
                path,
                message: T`${value} is not a valid string`,
            });

            return new ValidationResult(false, [message]);
        }

        const otherMessages = [];

        if (!isNil(allowedChars)) {
            // TODO escape allowedChars
            const regex = new RegExp(`^[${allowedChars}]*$`);
            if (!regex.test(value)) {
                const message = new ValidationMessage({
                    message: T`${path} may only contain ${allowedChars}.`,
                    path,
                });
                otherMessages.push(message);
            }
        }

        if (!isNil(minLen) && minLen > value.length) {
            const message = new ValidationMessage({
                message: T`${path} may not be shorter than ${minLen}.`,
                path,
            });
            otherMessages.push(message);
        }

        if (!isNil(maxLen) && maxLen < value.length) {
            const message = new ValidationMessage({
                message: T`${path} may not be longer than ${maxLen}.`,
                path,
            });
            otherMessages.push(message);
        }

        if (otherMessages.length > 0) {
            return new ValidationResult(false, otherMessages);
        }

        return new ValidationResult(true);
    },
    attributes: [
        {
            name: 'allowedChars',
            display: {
                en: 'Allowed Characters',
            },
            type: 'string',
            desc: {
                en: "A string of characters that are allowed in field's value.",
            },
        },
        {
            name: 'minLen',
            display: {
                en: 'Minimum Length',
            },
            desc: {
                en: "The minimum length of the field's value.",
            },
            type: 'number',
        },
        {
            name: 'maxLen',
            display: {
                en: 'Maximum Length',
            },
            desc: {
                en: "THe maximum length of the field's value.",
            },
            type: 'number',
        },
    ],
};
