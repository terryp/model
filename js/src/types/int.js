import { defaultComparators } from './baseComparators.js';
import { ValidationResult, ValidationMessage } from '../validationResult.js';
import { T } from '@omniblack/localization';
import { isNil } from 'lodash-es';

export const integer = {
    name: 'integer',
    comparators: defaultComparators('integer'),
    adapters: {
        // big ints are dangerous to convert to numbers
        // as they may lose precision
        string: {
            to: (value) => value.toString(10),
            from: (value) => BigInt(value),
        },
    },
    validator({ value, min, max }, path) {
        if (typeof value !== 'bigint') {
            const msg = new ValidationMessage({
                message:  T`${value} is not a valid integer.`,
                path,
            });
            return new ValidationResult(false, [msg]);
        }

        const otherMessages = [];

        if (!isNil(min) && min > value) {
            const message = new ValidationMessage({
                message: T`${path} must not be less than ${min}.`,
                path,
            });
            otherMessages.push(message);
        }

        if (!isNil(max) && max < value) {
            const message = new ValidationMessage({
                message: T`${path} must not be greater than ${max}.`,
                path,
            });
            otherMessages.push(message);
        }

        if (otherMessages.length > 0) {
            return new ValidationResult(false, otherMessages);
        }

        return new ValidationResult(true);
    },
    attributes: [
        {
            name: 'min',
            display: {
                en: 'Minimum',
            },
            desc: {
                en: "The minimum value of this field's value",
            },
            type: 'integer',
        },
        {
            name: 'max',
            display: {
                en: 'Maximum',
            },
            desc: {
                en: "The maximum value of this field's value.",
            },
            type: 'integer',
        },
    ],
};
