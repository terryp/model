Requirements:
    1. Organizer
        1. Basic Indiv
            1. Validation
            1. Empty Fields Removal
        1. Luxon date times
            1. min support
            1. max support
        1. string support
    1. Muninn
        1. Email Validation - Minimum support

    1. Meta model validation
        1. List support
        1. Runtime value insertion - stuff like valid types
