import { Code } from '@omniblack/error';
import { T } from '@omniblack/localization';

export function checkEnum({
    value,
    options,
    argName,
}) {
    if (!options.has(value)) {
        throw new Code(T`${value} is not a valid value for ${argName}`);
    }
}
