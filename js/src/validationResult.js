import { DotPath, deepFreeze } from '@omniblack/utils';
import { update, assign } from 'lodash-es';

/** @typedef {(Text|UIString)} localizable */

/**
 * @typedef {Object} ValidationMessageLike
 * @property {localizable} message - A friendly message describing the problem.
 * @property {DotPath[]|DotPath} paths - The paths to the fields
 * involved in the error.
 * @property {localizable[]} [suggestions] - Suggestions to help resolve the
 *      error.
 * @borrows ValidationMessageLike.paths as path
 * @memberof module:lib/model
 */

/**
 * Class representing a validation error.
 * @memberof module:lib/model
 */
export class ValidationMessage {
    /**
     * @constructor
     * @param args {Object}
     * @param {localizable} args.message - A friendly message
     *       describing the problem.
     * @param {(DotPath[]|DotPath)} args.paths - The paths to the fields
     *      involved in the error.
     * @borrows args.paths as args.path
     * @param {localizable[]} [args.suggestions] - Suggestions to help resolve the
     *      error.
     */
    constructor({ message, path, paths = path, suggestions }) {
        if (paths instanceof DotPath) {
            paths = [paths];
        }

        paths = paths.map((path) => {
            if (!(path instanceof DotPath)) {
                return DotPath.fromString(path);
            }
            return path;
        });
        this.message = message;
        this.paths = paths;
        this.suggestions = suggestions;
    }
}

/**
 * A validation result from the model validator.
 * @property {Boolean} valid - Is the field or record valid.
 * @property {Object} messagesByPath - The validation messages attached
 *      place in a structure similar to the record they refer to.
 *      TODO expand upon this when the shape is finalized
 * @property {ValidationMessage[]} messages - The validation messages.
 * @memberof module:lib/model
 */
export class ValidationResult {
    /**
     * @constructor
     * @param {Boolean} valid - Was validation successful.
     * @param {Array<ValidationMessage|ValidationMessageLike>} [messages] - The
     *      messages associated with the validation.
     */
    constructor(valid, messages = []) {
        this.valid = valid;
        this.messagesByPath = {};

        if (!Array.isArray(messages)) {
            messages = [messages];
        }

        const messagesCoerced = messages.map((message) => {
            if (!(message instanceof ValidationMessage)) {
                return new ValidationMessage(message);
            }
            return message;
        });

        for (const message of messagesCoerced) {
            const { paths } = message;
            for (const path of paths) {
                update(this.messagesByPath, path, (currentValue = []) => {
                    if (!Array.isArray(currentValue)) {
                        const oldValue = currentValue;
                        currentValue = [];
                        assign(currentValue, oldValue);
                    }

                    currentValue.push(message);
                    return currentValue;
                });
            }
        }

        this.messages = messagesCoerced;

        deepFreeze(this);
        return this;
    }

    merge(...otherResults) {
        let valid = this.valid;
        let newMessages = [...this.messages];

        for (const result of otherResults) {
            const {
                messages: otherMessages,
                valid: otherValid,
            } = result;
            valid = valid && otherValid;
            newMessages = newMessages.concat(otherMessages);
        }

        return new this.constructor(valid, newMessages);
    }
}

