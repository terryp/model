import { getStructName, getPath, getModel } from './Structs.js';
import iter from 'itertools/itertools.js';
const { ifilter } = iter;

/**
 * @param {indiv} indiv - The indiv to walk
 * @param {filters} filters - The filters used to control the walking
 */
export function *walk({
    indiv,
    filter,
    descend,
    skipped,
    __rootValue = indiv,
} = {}) {
    const model = getModel(indiv);
    const structName = getStructName(indiv);
    const structDef = model.structs.getDef(structName);
    const path = getPath(indiv);

    for (const fieldDef of structDef.fields) {
        const { name } = fieldDef;
        const fieldPath = path.append(name);

        const value = indiv[name];
        const yieldVal = {
            value,
            fieldDef,
            path: fieldPath,
            rootValue: __rootValue,
        };

        const visited = shouldVisit(yieldVal);

        const isChild = fieldDef.type === 'child';
        const descended = isChild && shouldDescend(yieldVal);

        // TODO should root value be a copy to block edits while recusing
        if (visited) {
            yield yieldVal;
        }


        if (descended) {
            yield* walk({
                indiv: value,
                filter,
                descend,
                skipped,
                __rootValue,
            });
        }

        if (!visited || (isChild && !descended)) {
            notifySkip({ ...yieldVal, visited, descended });
        }
    }


    function shouldVisit(...args) {
        if (filter) {
            return filter(...args);
        } else {
            return true;
        }
    }

    function shouldDescend(...args) {
        if (descend) {
            return descend(...args);
        } else {
            return true;
        }
    }

    function notifySkip(...args) {
        if (skipped) {
            skipped(...args);
        }
    }
}

export function *rWalk({ indiv, filter, descend, skipped: userSkipped } = {}) {
    const fields = Array.from(walk(indiv, { descend, skipped }));
    fields.reverse();

    const skips = new Map();

    yield* ifilter(fields, shouldVisit);

    function shouldVisit({ path, fieldDef, ...rest }) {
        let visited = true;
        if (filter) {
            visited = filter({ path, fieldDef, ...rest });
        }

        let descended = true;
        if (skips.has(String(path))) {
            descended = skips.get(String(path));
        }

        const isChild = fieldDef.type === 'child';
        if (!visited || (isChild && !descended)) {
            userSkipped({ fieldDef, path, ...rest });
        }

        return visited;
    }

    function skipped({ path, descended }) {
        skips.set(String(path), descended);
    }
}

export const visitLeaves = {
    filter({ fieldDef }) {
        return fieldDef.type !== 'child';
    },
};

