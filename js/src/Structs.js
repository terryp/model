import { isEqual, cloneDeep } from 'lodash-es';
import rxjs from 'rxjs';
import rxjsOperators from 'rxjs/operators/index.js';
import { Code } from '@omniblack/error';
import { T } from '@omniblack/localization';
import itertools from 'itertools';
import { DotPath, deepFreeze } from '@omniblack/utils';
const { map: imap } = itertools;

const customInspect = Symbol.for('nodejs.util.inspect.custom');

const { BehaviorSubject, combineLatest } = rxjs;
const { map, distinctUntilChanged } = rxjsOperators;


/*
 * We use global symbols so different versions of our library will
 * inter-operate without problem.
 */

// Subscribe to the validity of a field
const subscribeValidSymbol = Symbol.for('omniblack.model.subscribeValid');
//
// Subscribe to a value on the record
const subscribeValueSymbol = Symbol.for('omniblack.model.subscribeValue');


//  Get an observable for the validity of a record.
const getValidObsSymbol = Symbol.for('omniblack.model.getValidObs');

// Get an observable for the value of a record
const getValueObsSymbol = Symbol.for('omniblack.model.getValueObs');

// Set the value of an indiv
const setValueSymbol = Symbol.for('omniblack.model.setValue');

// The path from the root indiv to this child indiv
const pathSymbol = Symbol.for('omniblack.model.path');

// The name of the struct that this indiv is an instance of
const structNameSymbol = Symbol.for('omniblack.model.structName');

// The model the indiv was created from
const modelSymbol = Symbol.for('omniblack.model.model');

// Get the current validity of the indiv
const validSymbol = Symbol.for('omniblack.model.valid');

// The root property symbol
export const root = Symbol.for('omniblack.model.rootProperty');

// A bunch of our subscribe function work best if the subscription function
// is the last arg, but we want property to default to root.
// This function sorts out which is which and returns [property, subFunc];
function defaultToRoot(property, run) {
    if (typeof property === 'function' && run === undefined) {
        return [root, property];
    } else {
        return [property, run];
    }
}

export function subscribe(indiv, property, run) {
    const args = defaultToRoot(property, run);
    return indiv[subscribeValueSymbol](...args);
}


export function subscribeValid(indiv, property, run) {
    const args = defaultToRoot(property, run);
    return indiv[subscribeValidSymbol](...args);
}

export function validityObservable(indiv, property = root) {
    return indiv[getValidObsSymbol](property);
}

export function valueObservable(indiv, property = root) {
    return indiv[getValueObsSymbol](property);
}

export function getPath(indiv) {
    return indiv[pathSymbol];
}

export function getStructName(indiv) {
    return indiv[structNameSymbol];
}

export function getModel(indiv) {
    return indiv[modelSymbol];
}

export function getValid(indiv) {
    return indiv[validSymbol];
}

async function preprocess(struct_def, model) {
    for (const field of struct_def['fields']) {

    }
}

export function Structs(model) {
    if (!(this instanceof Structs)) {
        return new Structs(model);
    }

    const structClassCache = new Map();
    const structDefCache = new Map();

    function define(structDef) {
        const { name } = structDef;
        if (structDefCache.has(name)) {
            throw new Code(T`Struct "${name}" has already been defined.`);
        }

        const safeStructDef = deepFreeze(cloneDeep(structDef));

        structDefCache.set(name, safeStructDef);
        return true;
    }

    this.define = define;

    function getDef(structName) {
        return structDefCache.get(structName);
    }

    this.getDef = getDef;


    /**
     * Create a struct class.
     * @param {String} structName - The name of the struct to get the class for.
     * @returns {function}
     * @memberof module:lib/model
     */
    function get(structName) {
        if (structClassCache.has(structName)) {
            return structClassCache.get(structName);
        }

        const structDef = structDefCache.get(structName);

        if (!structDef) {
            throw new Code(T`Struct "${structName}" has not been defined.`);
        }

        const { fields } = structDef;


        Object.defineProperty(
            StructClass,
            Symbol.hasInstance,
            function(instance) {
                const name = getStructName(instance);
                return name === structName;
            },
        );

        function StructClass(obj = {}, path = new DotPath()) {
            if (!(this instanceof StructClass)) {
                return new StructClass(obj);
            }
            const self = this;

            const values = new Map();
            const valueSubjects = new Map();

            const valids = new Map();
            const validsSubjects = new Map();

            const names = new Set(fields.map(({ name }) => name));

            let valid = false;

            const propertiesArray = fields.map(
                ({ name, required, list, type, attrs }) => {
                    if (type !== 'child') {
                        return createNonChildField(
                            obj[name],
                            name,
                            required,
                            attrs,
                            type,
                            path.append(name),
                        );
                    } else {
                        return createChildField(
                            obj[name],
                            name,
                            required,
                            attrs,
                            type,
                            path.append(name),
                        );
                    }
                });

            const rootValidSubject = combineLatest(...validsSubjects.values())
                .pipe(
                    map((valids) => {
                        const [first, ...rest] = valids;
                        return first.merge(...rest);
                    }),
                    distinctUntilChanged(isEqual),
                );

            subscribeValid(root, (rootValidationResult) => {
                valid = rootValidationResult;
            });

            const rootValueSubject = combineLatest(
                ...imap(valueSubjects, ([key, value]) => {
                    return value.pipe(map((newValue) => [key, newValue]));
                }),
            ).pipe(map((newEntries) => Object.fromEntries(newEntries)));

            function subscribe(property, func) {
                checkProperty(property);

                if (property === root) {
                    const subscription = rootValueSubject.subscribe(func);
                    return subscription;
                } else {
                    const subject = valueSubjects.get(property);
                    const subscription = subject.subscribe(func);
                    return subscription;
                }
            }

            function subscribeValid(property, func) {
                checkProperty(property);

                if (property === root) {
                    const subscription = rootValidSubject.subscribe(func);
                    return subscription;
                } else {
                    const subject = validsSubjects.get(property);
                    const subscription = subject.subscribe(func);
                    return subscription;
                }
            }

            function getValidObs(property) {
                checkProperty(property);

                if (property === root) {
                    return rootValidSubject;
                } else {
                    return validsSubjects.get(property);
                }
            }

            function getValueObs(property) {
                checkProperty(property);

                if (property === root) {
                    return rootValueSubject;
                } else {
                    return valueSubjects.get(property);
                }
            }

            function set(newValue) {
                /*
                 * Invoke each fields setter so that validation and subscribers
                 * gets notified of the new values.
                 */
                for (const name of names) {
                    const newFieldValue = newValue[name];
                    self[name] = newFieldValue;
                }
            }

            function checkProperty(property) {
                if (property !== root && !names.has(property)) {
                    throw new Code(T`Unknown property "${property}".`);
                }
            }

            const metaProperties = [
                metaProperty(subscribeValidSymbol, subscribeValid),
                metaProperty(subscribeValueSymbol, subscribe),
                metaProperty(getValidObsSymbol, getValidObs),
                metaProperty(getValueObsSymbol, getValueObs),
                metaProperty(setValueSymbol, set),
                metaProperty(customInspect, () => Object.fromEntries(values)),
                metaProperty(pathSymbol, path),
                metaProperty(structNameSymbol, structName),
                metaProperty(modelSymbol, model),
                [
                    validSymbol,
                    {
                        enumerable: false,
                        configurable: false,
                        get() {
                            return valid;
                        },
                    },
                ],
            ];

            const properties = Object.fromEntries(
                propertiesArray.concat(metaProperties),
            );

            // Using a proxy would allow us to customize the error messages
            // and allow stuff like custom symbol keys only, but
            // proxies can't be jitted by most of the engines and really
            // slow down stuff.
            Object.defineProperties(self, properties);
            Object.preventExtensions(self);

            // Drop the reference to the original object
            // We don't need it any more
            obj = undefined;

            return self;

            function createNonChildField(
                value,
                name,
                required,
                attrs,
                type,
                path,
            ) {
                values.set(name, value);
                const valueSubject = new BehaviorSubject(value);
                valueSubjects.set(name, valueSubject);


                const valid = model.types.validate(
                    type,
                    {
                        value,
                        required,
                        ...attrs,
                    },
                    path,
                );

                valids.set(name, valid);
                const validSubject = new BehaviorSubject(valid);
                const validObs = validSubject.pipe(
                    distinctUntilChanged(isEqual),
                );
                validsSubjects.set(name, validObs);

                return [
                    name,
                    {
                        configurable: false,
                        enumerable: true,
                        set(newValue) {
                            const currentValue = values.get(name);
                            const sameValue = model.types.compare(
                                {
                                    type,
                                    value: newValue,
                                },
                                '==',
                                {
                                    type,
                                    value: currentValue,
                                },
                            );

                            if (!sameValue) {
                                values.set(name, newValue);
                                valueSubject.next(newValue);

                                const valid = model.types.validate(type, {
                                    value: newValue,
                                    required,
                                    ...attrs,
                                }, path);
                                valids.set(name, valid);
                                validSubject.next(valid);
                            }
                        },
                        get() {
                            return values.get(name);
                        },
                    },
                ];
            }

            function createChildField(
                value,
                name,
                required,
                attrs,
                type,
                path,
            ) {
                values.set(name, value);
                const Struct = get(attrs.structName);
                const indiv = new Struct(value, path);

                const valueSubject = valueObservable(indiv, root);
                valueSubjects.set(name, valueSubject);
                values.set(name, indiv);

                const validSubject = validityObservable(indiv, root);
                validsSubjects.set(name, validSubject);

                validSubject.subscribe((newResult) => {
                    valids.set(name, newResult);
                });

                return [name,
                    {
                        configurable: false,
                        enumerable: true,
                        set(newValue) {
                            // The child indiv will notify us if this changed
                            // the effective value,
                            // and of new validation results
                            indiv[setValueSymbol](newValue);
                        },
                        get() {
                            return indiv;
                        },
                    },
                ];
            }
        }

        structClassCache.set(structName, StructClass);
        return StructClass;
    }

    this.get = get;
}

// Meta properties are our private interface allowing our functions
// to access information about indivs
function metaProperty(name, value) {
    return [
        name,
        {
            value,
            configurable: false,
            writable: false,
            enumerable: false,
        },
    ];
}

