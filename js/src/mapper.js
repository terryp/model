import { walk, rWalk } from './walker.js';
import { set, defaultsDeep, get } from 'lodash-es';

function createMapper(walkFunc) {
    return function mapper({ indiv, filter, descend }, cb) {
        const newRec = {};

        for (const field of walkFunc({ indiv, filter, descend, skipped })) {
            const newValue = cb(field);
            const path = field.path;
            set(newRec, path, newValue);
        }

        function skipped({ path, value, visited, descended }) {
            if (!visited && descended) {
                const newValues = get(newRec, path);
                defaultsDeep(newValues, value);
            } else {
                set(newRec, path, value);
            }
        }
        return newRec;
    };
}


export const map = createMapper(walk);
export const rMap = createMapper(rWalk);


