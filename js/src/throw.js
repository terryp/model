import { Base } from '@omniblack/error';
import { ValidationResult } from './validationResult.js';
import { getValid } from './Structs.js';

export class InvalidRec extends Base {
    constructor(
        msg,
        {
            retry = false,
            notifyDev = true,
            details = {},
            indiv,
            validationResult,
        } = {},
    ) {
        const isResult = validationResult instanceof ValidationResult;

        if (validationResult && !isResult) {
            validationResult = new ValidationResult(
                validationResult.valid,
                validationResult.messages,
            );
        }

        super(msg, {
            retry,
            notifyDev,
            details,
        });

        Object.assign(this, validationResult);
    }
}

export function throwIfInvalid(indiv, msg) {
    const validationResult = getValid(indiv);
    if (!validationResult.valid) {
        throw new InvalidRec(msg, { validationResult, indiv });
    }
}
