import ava from 'ava';
import { Code } from '@omniblack/error';
import sinon from 'sinon';
import { Model } from '../src/Model.js';
import {
    validityObservable,
    valueObservable,
    getStructName,
    getPath,
} from '../src/Structs.js';

// The query string brings a second version of the module into memory
// eslint-disable-next-line import/no-unresolved
import { getStructName as getStructName2 } from '../src/Structs.js?testing';

const structNoChild = {
    name: 'noChild',
    display: {
        en: 'No Child',
    },
    fields: [
        {
            name: 'field1',
            type: 'string',
            display: {
                en: 'Field 1',
            },
            required: true,
        },
        {
            name: 'field2',
            type: 'number',
            display: {
                en: 'Field 2',
            },
            required: true,
        },
    ],
};

const structHasChildRoot = {
    name: 'root',
    display: {
        en: 'Root',
    },
    fields: [
        {
            name: 'childField',
            type: 'child',
            attrs: {
                structName: 'level1',
            },
            required: true,
        },
    ],
};

const structLevel1 = {
    name: 'level1',
    display: {
        en: 'Level 1',
    },
    fields: [
        {
            name: 'childField',
            type: 'child',
            attrs: {
                structName: 'level2',
            },
            required: true,
        },
    ],
};

const structLevel2 = {
    name: 'level2',
    display: {
        en: 'Level 2',
    },
    fields: [
        {
            name: 'leafField',
            type: 'string',
            required: true,
        },
    ],
};



ava('loads Struct', (t) => {
    const model = new Model();
    t.true(model.structs.define(structNoChild));
    const Struct = model.structs.get('noChild');
    t.assert(Struct instanceof Function, 'Returns a function');

    const cachedCopy = model.structs.get('noChild');

    t.is(cachedCopy, Struct, 'Returns a cached struct.');

    t.throws(() => model.structs.define(structNoChild), {
        instanceOf: Code,
    });
});

ava('Indiv correct name enforcement', async (t) => {
    const model = new Model();
    model.structs.define(structNoChild);

    const testStruct = model.structs.get('noChild');

    const indiv = testStruct();

    t.throws(
        () => {
            indiv.bogusField = 1;
        },
        {
            instanceOf: TypeError,
        },
    );

    t.notThrows(() => {
        indiv.field1 = 1;
    });
});

ava('Validity observable', (t) => {
    const model = new Model();
    model.structs.define(structNoChild);

    const Struct = model.structs.get('noChild');
    const indiv = new Struct();

    const validObs = validityObservable(indiv);
    const spy = sinon.spy();

    const subscription = validObs.subscribe(spy);
    t.teardown(() => subscription.unsubscribe());

    indiv.field1 = 'field1';
    indiv.field2 = 1;

    t.is(spy.callCount, 3, 'Validity changed three times');

    t.true(
        spy.firstCall.calledWithMatch(
            sinon.match({ valid: false }),
        ),
        'Indiv started invalid',
    );

    t.true(
        spy.secondCall.calledWithMatch(
            sinon.match({ valid: false }),
        ),
        'Indiv remains invalid',
    );

    t.true(
        spy.thirdCall.calledWithMatch(
            sinon.match({ valid: true }),
        ),
        'Indiv becomes valid',
    );
});

ava('Value observable', (t) => {
    const model = new Model();
    model.structs.define(structNoChild);

    const Struct = model.structs.get('noChild');

    const indiv = new Struct();

    const valueObs = valueObservable(indiv);
    const spy = sinon.spy();
    const subscription = valueObs.subscribe(spy);
    t.teardown(() => subscription.unsubscribe);

    indiv.field1 = 'set 1';
    indiv.field1 = 'set 1';

    indiv.field2 = 2;
    indiv.field2 = 2;

    // Note 4 assigns but 3 changes.
    // 2 of the assignment should be ignored since they assign an
    // equal value. One change for the initial value, and 2 for the
    // not ignored changes
    t.is(spy.callCount, 3, 'Value changed three times');
    t.true(
        spy.firstCall.calledWith({ field1: undefined, field2: undefined }),
        'Indiv started empty',
    );

    t.true(
        spy.secondCall.calledWith({ field1: 'set 1', field2: undefined }),
        'Second call field 1 is set',
    );

    t.true(
        spy.thirdCall.calledWith({ field1: 'set 1', field2: 2 }),
        'Third call all fields are set',
    );
});

ava('Cross version symbol api', (t) => {
    const model = new Model();

    model.structs.define(structNoChild);

    const Struct = model.structs.get('noChild');

    const indiv = new Struct();

    t.not(getStructName, getStructName2, 'Two versions have been loaded');

    t.is(getStructName(indiv), 'noChild', 'Original version works');

    t.is(getStructName2(indiv), 'noChild', 'Second version works');
});

ava('Path is set correctly', (t) => {
    const model = new Model();

    model.structs.define(structLevel1);
    model.structs.define(structLevel2);
    model.structs.define(structHasChildRoot);

    const Struct = model.structs.get('root');

    const indiv = new Struct();

    t.is(
        getPath(indiv.childField.childField).toString(),
        'childField.childField',
        'The child path is correctly set on deep children',
    );
});
