import { inspect } from 'util';

import ava from 'ava';
import { Code } from '@omniblack/error';
import { Model } from '../src/Model.js';
import { coerceFrom, coerceTo } from '../src/coercedCopy.js';
import { InvalidRec } from '../src/throw.js';

inspect.defaultOptions.depth = Infinity;
inspect.defaultOptions.colors = true;

const date = new Date();

const structDef = {
    name: 'struct',
    display: {
        en: 'Struct',
    },
    fields: [
        {
            name: 'field1',
            type: 'string',
            display: {
                en: 'Field 1',
            },
            required: true,
        },
        {
            name: 'field2',
            type: 'date',
            display: {
                en: 'Field 2',
            },
            required: true,
        },
    ],
};

const rec = {
    field1: 'test',
    field2: date,
};

const stringForm = {
    field1: 'test',
    field2: date.toISOString(),
};

const recInvalid = {
    field1: new Date(),
    field2: 'test',
};

ava.beforeEach((t) => {
    t.context.model = new Model();
    t.context.model.structs.define(structDef);
});



ava('Coerces valid indivs', (t) => {
    const Struct = t.context.model.structs.get('struct');

    const indiv = new Struct(rec);

    const stringForm = coerceTo(indiv, 'string');

    t.deepEqual(stringForm, stringForm, 'Converts to string form');
});

ava('Throws on invalid indivs', (t) => {
    const Struct = t.context.model.structs.get('struct');

    const indiv = new Struct(recInvalid);

    t.throws(() => coerceTo(indiv, 'string'), {
        instanceOf: InvalidRec,
    }, 'coerceFrom throw on invalid indiv');
});


ava('Coerces from string from', (t) => {
    const { model } = t.context;
    const expectedIndiv = model.structs.get('struct')(rec);
    const indiv = coerceFrom(stringForm, 'string', 'struct', model);

    t.deepEqual(indiv, expectedIndiv, 'Correctly coerces to an indiv');
});

