import { fs } from 'fs/promises';

import { safeLoad, safeDump } from 'js-yaml';

import { coerceFrom, coerceTo } from '@omniblack/model';


export async function loadFile(model, structName, filePath) {
    const fileStr = await fs.readFile(filePath);
    const rec = safeLoad(fileStr, { filename: filePath });
    return coerceFrom(rec, 'yaml', structName, model);
}

const defaultDumpOpts = {
    indent: 4,
    flowLevel: -1,
    sortKeys: true,
    noCompatMode: true,
};

export async function dumpFile(indiv, filePath, dumpOpts = defaultDumpOpts) {
    const rec = coerceTo(indiv, 'yaml');
    const fileStr = safeDump(rec, defaultDumpOpts);
    await fs.writeFile(filePath, fileStr);
}

