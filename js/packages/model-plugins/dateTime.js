import { DateTime } from 'luxon';
import { T } from '@omniblack/localization';
import {
    ValidationMessage,
    ValidationResult,
    valueOfComparators,
} from '@omniblack/model';

export const luxonDatetime = {
    name: 'datetime',
    adapters: {
        string: {
            to: (value) => value.toISO(),
            from: (value) => DateTime.fromISO(value),
        },
    },

    comparators: valueOfComparators(new Set('datetime', 'date')),
    validator({ value, min, max }, path) {
        if (!DateTime.isDateTime(value)) {
            const msg = new ValidationMessage({
                message: T`${value} is not a valid luxon date.`,
                path,
            });
            return new ValidationResult(false, [msg]);
        }

        if (!value.isValid) {
            const msg = new ValidationMessage({
                message: T`${value} is not a valid date,
                     because ${value.invalidExplanation}.`,
                path,
            });
            return new ValidationResult(false, [msg]);
        }

        const otherMessages = [];

        if (min && min > value) {
            const msg = new ValidationMessage({
                message: T`${value} must not earlier than ${min}`,
                path,
            });
            otherMessages.push(msg);
        }

        if (max && max < value) {
            const msg = new ValidationMessage({
                message: T`${value} must not be later than ${max}.`,
                path,
            });
            otherMessages.push(msg);
        }

        if (otherMessages.length > 0) {
            return new ValidationResult(false, otherMessages);
        }

        return new ValidationResult(true);
    },
    attributes: [
        {
            name: 'min',
            display: {
                en: 'Minimum Datetime',
            },
            type: 'datetime',
        },
        {
            name: 'max',
            display: {
                en: 'Maximum Datetime',
            },
            type: 'datetime',
        },
    ],
};
