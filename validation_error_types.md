# Validation error types

1. Model Error: The model contains an invalid setup like un-comparable types.
1. Invalid Value: The value provided is not of the correct type.
1. Coercion Error: The value provided could not be coerced to/from the
    specified format.
1. Constraint Error: A constraint was violated. (Ex. min or max)
