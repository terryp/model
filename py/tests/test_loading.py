from pytest import fixture, mark

from omniblack.model import Model

pytestmark = mark.anyio


@fixture
def model():
    model = Model(__name__)
    print(model)
    return model


meta_names = {
    'struct',
    'field',
    'ui_string',
    'implementation_details',
    'python_implementation_details',
}


async def test_load_from_package(model):
    assert not (set(model.structs) & meta_names)
    await model.structs.load_model_package('omniblack.model.structs')
    assert len(set(model.structs) & meta_names) == len(meta_names)
